

--<<item_break>>--

### Item text:

Ik had op schooldagen een vaste ochtendroutine (dat wil zeggen, ik deed meestal elke dag hetzelfde om me klaar te maken).   [[uiid:item_1]]

### Think-aloud notes:



### Probes:

Wat versta je onder 'schooldagen'?   [[mqid:probes_1]]



Wat denk je dat er wordt verstaan onder 'vaste routine', dat je meestal elke dag hetzelfde deed om je klaar te maken?   [[mqid:probes_2]]






--<<item_break>>--

### Item text:

Mijn ouders waren vaak te laat om me op te halen (bijvoorbeeld van school, nazorg of sport).   [[uiid:item_2]]

### Think-aloud notes:



### Probes:

Wat versta je onder 'te laat'?   [[mqid:probes_3]]



Wat betekent 'ophaalden' voor jou?   [[mqid:probes_4]]






--<<item_break>>--

### Item text:

Mijn ouders hielden bij wat ik at (ze zorgden er bijvoorbeeld voor dat ik geen maaltijden oversloeg of probeerden ervoor te zorgen dat ik gezond at).   [[uiid:item_3]]

### Think-aloud notes:



### Probes:

Wat betekent 'bijhielden' voor jou?   [[mqid:probes_5]]






--<<item_break>>--

### Item text:

Mijn familie at de meeste dagen samen.   [[uiid:item_4]]

### Think-aloud notes:



### Probes:

Wat versta je onder 'familie'?   [[mqid:probes_6]]



Wat betekent 'de meeste dagen van de week'?   [[mqid:probes_7]]



Wat betekent  '"samen eten'" voor jou?   [[mqid:probes_8]]






--<<item_break>>--

### Item text:

Mijn ouders probeerden ervoor te zorgen dat ik een goede nachtrust had ik haddenbijvoorbeeld een normale bedtijd, mijn ouders controleerden of ik ging slapen).   [[uiid:item_5]]

### Think-aloud notes:



### Probes:

Wat betekent  ''zorgden ervoor' voor jou?   [[mqid:probes_9]]



Wat betekent een 'goede nachtrust' voor je?   [[mqid:probes_10]]






--<<item_break>>--

### Item text:

Ik had een bedtijdroutine (mijn ouders stopten me bijvoorbeeld in, mijn ouders lazen me een boek voor, ik nam een bad).   [[uiid:item_6]]

### Think-aloud notes:



### Probes:

Wat betekent het woord "bedtijdroutine" voor je? Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_11]]






--<<item_break>>--

### Item text:

In mijn naschoolse of vrije uren wist tenminste ��n van mijn ouders wat ik deed.   [[uiid:item_7]]

### Think-aloud notes:



### Probes:

Wat betekenen de woorden "naschoolse" of "vrije" uren voor jou?   [[mqid:probes_12]]



Wat versta je onder ' wist wat ik deed'?   [[mqid:probes_13]]






--<<item_break>>--

### Item text:

Ik wist meestal wanneer mijn ouders thuis  zouden komen.   [[uiid:item_8]]

### Think-aloud notes:



### Probes:

Wat versta je onder ' wist wanneer dat zij thuis zouden komen'?   [[mqid:probes_14]]



Wat versta je onder "meestal?"   [[mqid:probes_15]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders controleerde regelmatig of ik mijn huiswerk had gemaakt.   [[uiid:item_9]]

### Think-aloud notes:



### Probes:

Wat betekent het woord "controleren"? Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_16]]



Wat versta je onder "regelmatig"   [[mqid:probes_17]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders hield regelmatig mijn schoolvoortgang bij.   [[uiid:item_10]]

### Think-aloud notes:



### Probes:

Wat betekent het woord "schoolvoortgang" voor je?   [[mqid:probes_18]]



Wat betekent het woord "bijhielden" voor je? Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_19]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders strafte onvoorspelbaar   [[uiid:item_11]]

### Think-aloud notes:



### Probes:

Wat betekent het woord "onvoorspelbaar"? Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_20]]



Wat versta je onder 'straffen"?Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_21]]






--<<item_break>>--

### Item text:

Ik vroeg me vaak af of een van mijn ouders aan het eind van de dag wel of niet thuis zou komen.   [[uiid:item_12]]

### Think-aloud notes:



### Probes:

Wat betekent ' vaak' voor jou?   [[mqid:probes_22]]



Wat betekent het woord "einde van de dag"'?   [[mqid:probes_23]]






--<<item_break>>--

### Item text:

Er kwamen en gingen vaak mensen in mijn huis waarvan ik niet had verwacht dat ze daar zouden zijn.   [[uiid:item_13]]

### Think-aloud notes:



### Probes:

Kun je me vertelllen of het item van toepassing is op jou?   [[mqid:probes_24]]



Wat betekent ' vaak' voor jou?   [[mqid:probes_25]]



Wat betekent niet hadden verwacht voor jou?   [[mqid:probes_26]]






--<<item_break>>--

### Item text:

Minstens ��n ouder maakte elke dag tijd om te kijken hoe het met me ging.   [[uiid:item_14]]

### Think-aloud notes:



### Probes:

Kun je me vertellen wat je verstaat onder "kijken hoe het met me ging"?   [[mqid:probes_27]]



Wat betekent 'tijd (vrij) maken'?   [[mqid:probes_28]]






--<<item_break>>--

### Item text:

Mijn gezin plande activiteiten om samen te doen.   [[uiid:item_15]]

### Think-aloud notes:



### Probes:

Kun je me vertellen wat je verstaat onder "activiteiten"?   [[mqid:probes_29]]



Wat versta je onder 'plannen'?   [[mqid:probes_30]]



Wat versta je onder 'samen'?   [[mqid:probes_31]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders zou iets voor het gezin plannen, maar het plan dan niet uitvoeren.   [[uiid:item_16]]

### Think-aloud notes:



### Probes:

Kun je me verstellen wat je verstaat onder "iets voor het gezin plannen"?   [[mqid:probes_32]]



Kun je me vertellen wat je verstaat onder "maar het plan dan niet uitvoeren"?   [[mqid:probes_33]]






--<<item_break>>--

### Item text:

Mijn familie had vakantietradities die we elk jaar deden (bijv. een speciaal gerecht koken in een bepaalde tijd van het jaar / het huis op dezelfde manier versieren).   [[uiid:item_17]]

### Think-aloud notes:



### Probes:

Wat betekent het woord "vakantietraditie" voor je? Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_34]]






--<<item_break>>--

### Item text:

Er was een lange periode dat ik een van mijn ouders niet zag (bijv. Militaire inzet, gevangenisstraf, voogdijregelingen).   [[uiid:item_18]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_35]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_36]]



Wat versta je onder "lange periode"?   [[mqid:probes_37]]



Wat versta je onder een van de ouders 'niet zien' ?   [[mqid:probes_38]]






--<<item_break>>--

### Item text:

Ik heb veranderingen ervaren in mijn voogdijregeling.   [[uiid:item_19]]

### Think-aloud notes:



### Probes:

Ik wil dat je me vertelt of het item van toepassing is voor jou?   [[mqid:probes_39]]



Wat betekent het woord "voogdijregeling" voor je? Als je de keuze had zou je het woord dan laten staan of wijzigen in iets anders?   [[mqid:probes_40]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_41]]






--<<item_break>>--

### Item text:

Ik ben vaak verhuisd   [[uiid:item_20]]

### Think-aloud notes:



### Probes:

Wat verta je onder "verhuisd"?   [[mqid:probes_42]]



Wat versta je onder "vaak"?   [[mqid:probes_43]]






--<<item_break>>--

### Item text:

Minstens een van zijn ouders veranderde regelmatig van baan.   [[uiid:item_21]]

### Think-aloud notes:



### Probes:

Ik wil dat je me vertelt of het item van toepassing is voor jou?   [[mqid:probes_44]]



Wat versta je onder "baan veranderen"?   [[mqid:probes_45]]



Wat versta je onder "regelmatig"?   [[mqid:probes_46]]






--<<item_break>>--

### Item text:

Er waren momenten dat een van mijn ouders werkloos was en geen baan kon vinden, hoewel hij/zij er wel een wilde hebben.   [[uiid:item_22]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_47]]



Heb je je gefocust op het eerste deel (�een van mijn ouders werkloos was. . .�) of liever op het tweede deel (�. . . hoewel zij er een wilde hebben. . . . �)?   [[mqid:probes_48]]



Wat versta je onder "momenten"?   [[mqid:probes_49]]



Wat versta je onder werkloos en geen baan kunnen vinden?   [[mqid:probes_50]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_51]]






--<<item_break>>--

### Item text:

Er was een periode dat ik me vaak zorgen maakte dat ik niet genoeg te eten zou hebben.   [[uiid:item_23]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_52]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_53]]



Wat versta je onder "een periode"?   [[mqid:probes_54]]



Wat versta je onder 'zorgen maken' ?   [[mqid:probes_55]]






--<<item_break>>--

### Item text:

Er was een periode dat ik me vaak zorgen maakte dat mijn gezin niet genoeg geld zou hebben om noodzakelijke dingen als kleding of rekeningen te betalen.   [[uiid:item_24]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_56]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_57]]



Wat versta je onder "een periode"?   [[mqid:probes_58]]



Kun je me vertellen wat je denkt dat ze proberen te bereiken met deze vraag?   [[mqid:probes_59]]






--<<item_break>>--

### Item text:

Er was een tijd dat ik me thuis niet veilig voelde.   [[uiid:item_25]]

### Think-aloud notes:



### Probes:

Wat versta je onder "niet veilig voelen"?   [[mqid:probes_60]]



Wat versta je onder "een tijd was?   [[mqid:probes_61]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_62]]






--<<item_break>>--

### Item text:

Ik veranderde vaak van school.   [[uiid:item_26]]

### Think-aloud notes:



### Probes:

Wat versta je onder "vaak"?   [[mqid:probes_63]]



Wat versta je onder "school"?   [[mqid:probes_64]]



Wat versta je onder "veranderen"?   [[mqid:probes_65]]






--<<item_break>>--

### Item text:

Ik ben halverwege het jaar van school veranderd.   [[uiid:item_27]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_66]]



Wat versta je onder "halverwege het jaar"?   [[mqid:probes_67]]






--<<item_break>>--

### Item text:

Mijn ouders hadden een stabiele relatie met elkaar.   [[uiid:item_28]]

### Think-aloud notes:



### Probes:

Wat betekent het woord 'stabiele relatie' voor je?   [[mqid:probes_68]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_69]]






--<<item_break>>--

### Item text:

Mijn ouders zijn gescheiden.   [[uiid:item_29]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_70]]



Wat versta je onder 'gescheiden'?   [[mqid:probes_71]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_72]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders had veel romantische partners   [[uiid:item_30]]

### Think-aloud notes:



### Probes:

Kun je me vertellen of het item van toepassing is op jou?   [[mqid:probes_73]]



Wat betekent het woord "romantische" voor je?   [[mqid:probes_74]]



Wat betekent het woord "partners" voor je?   [[mqid:probes_75]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_76]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders was ongeorganiseerd.   [[uiid:item_31]]

### Think-aloud notes:



### Probes:

Wat betekent het woord 'ongeorganiseerd' voor je?   [[mqid:probes_77]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_78]]






--<<item_break>>--

### Item text:

Minstens een van mijn ouders was onvoorspelbaar.   [[uiid:item_32]]

### Think-aloud notes:



### Probes:

Wat versta je onder het woord "onvoorspelbaar"?   [[mqid:probes_79]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_80]]






--<<item_break>>--

### Item text:

Voor tenminste een van mijn ouders, toen ze van streek waren, wist ik niet hoe ze zouden reageren   [[uiid:item_33]]

### Think-aloud notes:



### Probes:

Wat betekenen de woorden 'van streek waren' voor je?   [[mqid:probes_81]]



Wat je versta je onder "reageren"   [[mqid:probes_82]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_83]]






--<<item_break>>--

### Item text:

Een van mijn ouders kon in een oogwenk van kalm naar woedend gaan.   [[uiid:item_34]]

### Think-aloud notes:



### Probes:

Wat betekenen de woorden 'in een oogwenk' voor je?   [[mqid:probes_84]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_85]]






--<<item_break>>--

### Item text:

Een van mijn ouders zou in een oogwenk van kalm naar gestrest of nerveus kunnen gaan.   [[uiid:item_35]]

### Think-aloud notes:



### Probes:

Wat betekenen de woorden 'in een oogwenk' voor je?   [[mqid:probes_86]]



Denk je dat het ok� is om over te praten in een interview, of is het te ongemakkelijk?   [[mqid:probes_87]]






--<<item_break>>--

### Item text:

Ik woonde in een schoon huis.   [[uiid:item_36]]

### Think-aloud notes:



### Probes:

Wat versta je onder "schoon"?   [[mqid:probes_88]]






--<<item_break>>--

### Item text:

Ik woonde in een rommelig huis (bijv. overal stapels spullen).   [[uiid:item_37]]

### Think-aloud notes:



### Probes:

Wat versta je onder "rommelig"?   [[mqid:probes_89]]






--<<item_break>>--

### Item text:

In mijn huis waren dingen die ik nodig had vaak op verschillende plaatsen weggelegd, zodat ik ze niet kon vinden.   [[uiid:item_38]]

### Think-aloud notes:



### Probes:

Heb je je gefocust op het eerste deel (�in mijn huis waren dingen . .�) of liever op het tweede deel (�. . . op verschillende plaatsen. . . . �)?   [[mqid:probes_90]]



Wat versta je onder "verschillende plaatsen"?   [[mqid:probes_91]]



Wat versta je onder "vaak"?   [[mqid:probes_92]]



Wat versta je onder "dingen"?   [[mqid:probes_93]]





